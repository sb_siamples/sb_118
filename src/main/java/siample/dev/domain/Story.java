package siample.dev.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//@Table(name = "STORY")
@Entity
public class Story {

	@Id
	//@GeneratedValue magában nem elég! Mert akkor nem insertál, kéne neki egy id érték
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	// @Column(columnDefinition = "TEXT")
	private String content;
	private Date   posted;

	@ManyToOne
	private Blogger blogger;

	public Story() {
	}
	
	public Story(Long id, String content, Date posted, Blogger blogger) {
		this.id = id;
		this.content = content;
		this.posted = posted;
		this.blogger = blogger;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getPosted() {
		return posted;
	}

	public void setPosted(Date posted) {
		this.posted = posted;
	}

	public Blogger getBlogger() {
		return blogger;
	}

	public void setBlogger(Blogger blogger) {
		this.blogger = blogger;
	}

	@Override
	public String toString() {
		return "Story [id=" + id + ", content=" + content + ", posted=" + posted + ", blogger=" + blogger + "]";
	}

}
