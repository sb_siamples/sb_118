package siample.dev.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

// @Table(name = "BLOGGER")
@Entity
public class Blogger {

	@Id
    //@GeneratedValue magában nem elég! Mert akkor nem insertál, kéne neki egy id érték
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String name;

	private int age;

	@OneToMany(mappedBy = "blogger")
	private List<Story> stories;
	// Legyen List, mert dbresult csak List-ként tud visszaadni

	public Blogger() {

	}

	public Blogger(Long id, String name, int age, List<Story> stories) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.stories = stories;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Blogger [id=" + id + ", name=" + name + ", age=" + age + "]";
	}

	public List<Story> getStories() {
		return stories;
	}

	public void setStories(List<Story> stories) {
		this.stories = stories;
	}

}
