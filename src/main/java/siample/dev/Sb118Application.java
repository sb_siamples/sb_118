package siample.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sb118Application {

	public static void main(String[] args) {
		SpringApplication.run(Sb118Application.class, args);
	}

}
